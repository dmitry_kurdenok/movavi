#ifndef PYRAMID_H
#define PYRAMID_H

#include <QMainWindow>
#include <QFileDialog>
#include <QPixmap>

#include "description.h"

namespace Ui {
class Pyramid;
}

class Pyramid : public QMainWindow
{
    Q_OBJECT

public:
    explicit Pyramid(QWidget *parent = 0);
    ~Pyramid();

private slots:
    void on_actionOpen_triggered();

    void on_comboBoxLayer_currentIndexChanged(int index);

    void on_comboBoxFile_currentIndexChanged(int index);

    void on_lineEdit_editingFinished();

private:
    Ui::Pyramid *ui;

    ImageList image_list;
    double coefficient;

    void openPicture(const QString &filename);
    void changePicture(int picture_index);
    void changeLayer(int picture_index, int layer_index);
};

#endif // PYRAMID_H
