#include "description.h"

ImageDescription::ImageDescription(const QString &_fn, const QString &_sn, const QPixmap &_pm) :
    full_name(_fn),
    short_name(_sn),
    pyramid_repr(_pm)
{
}

const QString &ImageDescription::getFullName() const
{
    return full_name;
}

const QString &ImageDescription::getShortName() const
{
    return short_name;
}

const PyramidRepresentation &ImageDescription::getPyramid() const
{
    return pyramid_repr;
}

bool ImageDescription::operator < (const ImageDescription &x) const
{
    return getDiagonal() < x.getDiagonal();
}

double ImageDescription::getDiagonal() const
{
    int w = getWidth();
    int h = getHeight();
    return sqrt(w*w + h*h);
}

int ImageDescription::getWidth() const
{
    return getPyramid().getOriginalSize().width();
}

int ImageDescription::getHeight() const
{
    return getPyramid().getOriginalSize().height();
}

ImageList::ImageList()
{
}

ImageList::~ImageList()
{
    for (int i = 0; i < data.size(); ++i)
        delete data[i];
}

void ImageList::add(ImageDescription *new_item)
{
    data.push_back(new_item);
    std::sort(data.begin(), data.end());
}

const ImageDescription &ImageList::operator [] (int index) const
{
    return *data[index];
}

int ImageList::size() const
{
    return data.size();
}

bool ImageList::containFullName(const QString &name) const
{
    return findFullName(name) != data.size();
}
bool ImageList::containShortName(const QString &name) const
{
    return findShortName(name) != data.size();
}

int ImageList::findFullName(const QString &name) const
{
    int index = 0;
    for (; index < data.size(); ++index)
        if (data[index]->getFullName() == name)
            break;
    return index;
}
int ImageList::findShortName(const QString &name) const
{
    int index = 0;
    for (; index < data.size(); ++index)
        if (data[index]->getShortName() == name)
            break;
    return index;
}
