#include "pyramid.h"
#include "ui_pyramid.h"

Pyramid::Pyramid(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Pyramid)
{
    ui->setupUi(this);
    ui->scrollArea->setWidget(ui->label);
    ui->lineEdit->setText(QString::number(PyramidRepresentation::getCoefficient()));
}

Pyramid::~Pyramid()
{
    delete ui;
}

void Pyramid::openPicture(const QString &filename)
{
    if (image_list.containFullName(filename))
    {
        ui->comboBoxFile->setCurrentIndex(image_list.findFullName(filename));
    }
    else
    {
        QPixmap pixmap(filename);
        if (pixmap.isNull())
        {
            ui->label->setText(tr("File is corrupted or have unsupported format!"));
            ui->comboBoxFile->setCurrentIndex(-1);
            ui->comboBoxLayer->clear();
            ui->labelSize->setText(QString(tr("Size: w x h")));
        }
        else
        {
            QString short_name = filename.split("/").last();
            if (image_list.containShortName(short_name))
            {
                QString base_name = short_name;
                int index = 2;
                while (image_list.containShortName(base_name + QString::number(index))) ++index;
                short_name = base_name + QString::number(index);
            }
            image_list.add(new ImageDescription(filename, short_name, pixmap));

            ui->comboBoxFile->clear();
            for (int i = 0; i < image_list.size(); ++i)
                ui->comboBoxFile->addItem(image_list[i].getShortName());
            ui->comboBoxFile->setCurrentIndex(image_list.findShortName(short_name));
        }
    }
}

void Pyramid::changePicture(int picture_index)
{
    ui->comboBoxLayer->clear();
    ui->comboBoxLayer->addItem(tr("0 (original)"));
    for (int i = 1; i < image_list[picture_index].getPyramid().getLayersCount(); ++i)
        ui->comboBoxLayer->addItem(QString::number(i));
    ui->comboBoxLayer->setCurrentIndex(0);
}

void Pyramid::changeLayer(int picture_index, int layer_index)
{
    ui->label->setPixmap(image_list[picture_index].getPyramid().getScaledLayer(layer_index));
    QSize size = image_list[picture_index].getPyramid().getLayer(layer_index).size();
    QString str_w = QString::number(size.width());
    QString str_h = QString::number(size.height());
    ui->labelSize->setText(QString(tr("Size: ")) + str_w + "x" + str_h);
}

void Pyramid::on_actionOpen_triggered()
{
    QString dialog_header = tr("Open File");
    QString dialog_filter = tr("Images (*.png *.jpg)");
    QString filename = QFileDialog::getOpenFileName(this, dialog_header, "", dialog_filter);
    if (!filename.isEmpty())
        openPicture(filename);
}

void Pyramid::on_comboBoxLayer_currentIndexChanged(int index)
{
    if (index != -1)
        changeLayer(ui->comboBoxFile->currentIndex(), index);
}

void Pyramid::on_comboBoxFile_currentIndexChanged(int index)
{
    if (index != -1)
        changePicture(index);
}


void Pyramid::on_lineEdit_editingFinished()
{
    bool is_double;
    double coef = ui->lineEdit->text().toDouble(&is_double);
    if (is_double && coef > 1.01)
    {
        PyramidRepresentation::setCoefficient(coef);
        changePicture(ui->comboBoxFile->currentIndex());
    }
    else
    {
        ui->lineEdit->setText(QString::number(PyramidRepresentation::getCoefficient()) + tr(" (value should be float more than 1.01)"));
    }
}
