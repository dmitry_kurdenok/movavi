#ifndef REPRESENTATION_H
#define REPRESENTATION_H

#include <QPixmap>
#include <QException>

class PyramidRepresentation
{
public:
    PyramidRepresentation(const QPixmap &);

    void setOriginal(const QPixmap &);

    static void setCoefficient(double);
    static double getCoefficient();

    const QPixmap &getOriginal() const;
    QPixmap getLayer(int index) const;

    QPixmap getScaledLayer(int index) const;

    int getLayersCount() const;
    QSize getOriginalSize() const;

private:
    QPixmap original;
    static double coefficient;

    class Exception : public QException
    {
    public:
        void raise() { throw *this; }
        Exception *clone() { return new Exception(*this); }
    };
};

#endif // REPRESENTATION_H
