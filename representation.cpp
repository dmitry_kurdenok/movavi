#include "representation.h"

PyramidRepresentation::PyramidRepresentation(const QPixmap &source)
{
    setOriginal(source);
}

void PyramidRepresentation::setOriginal(const QPixmap &source)
{
    original = source;
}

void PyramidRepresentation::setCoefficient(double coef)
{
    coefficient = coef;
}

double PyramidRepresentation::getCoefficient()
{
    return coefficient;
}

const QPixmap &PyramidRepresentation::getOriginal() const
{
    return original;
}

QPixmap PyramidRepresentation::getLayer(int index) const
{
    QSize current_size = original.size();
    for (int i = 0; i < index; ++i)
        current_size /= coefficient;
    return original.scaled(current_size);
}

QPixmap PyramidRepresentation::getScaledLayer(int index) const
{
    return getLayer(index).scaled(getOriginal().size());
}

int PyramidRepresentation::getLayersCount() const
{
    QSize old_size = original.size();
    QSize current_size = old_size/coefficient;
    int layers_count = 1;
    while (current_size != old_size)
    {
        old_size = current_size;
        current_size = old_size/coefficient;
        ++layers_count;
    }
    return layers_count;
}

QSize PyramidRepresentation::getOriginalSize() const
{
    return getOriginal().size();
}

double PyramidRepresentation::coefficient(2.0);
