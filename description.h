#ifndef DESCRIPTION_H
#define DESCRIPTION_H

#include <QSetIterator>
#include <QtMath>

#include "representation.h"

class ImageDescription
{
public:
    ImageDescription(const QString &_fn, const QString &_sn, const QPixmap &_pm);

    const QString &getFullName() const;
    const QString &getShortName() const;
    const PyramidRepresentation &getPyramid() const;

    bool operator < (const ImageDescription &x) const;

    double getDiagonal() const;
    int getWidth() const;
    int getHeight() const;

private:
    QString full_name;
    QString short_name;
    PyramidRepresentation pyramid_repr;

};

class ImageList
{
public:
    ImageList();
    ~ImageList();

    void add(ImageDescription *);

    const ImageDescription &operator [] (int) const;

    int size() const;

    bool containFullName(const QString &) const;
    bool containShortName(const QString &) const;

    int findFullName(const QString &) const;
    int findShortName(const QString &) const;

private:
    QVector<ImageDescription *> data;
};

#endif // DESCRIPTION_H
